/*
 * System-C TLM-2.0 remoteport wires.
 *
 * Copyright (c) 2016-2018 Xilinx Inc
 * Written by Edgar E. Iglesias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define SC_INCLUDE_DYNAMIC_PROCESSES

#include <inttypes.h>
#include <sys/utsname.h>

#include "systemc.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/tlm_quantumkeeper.h"
#include <iostream>

extern "C" {
#include "safeio.h"
#include "remote-port-proto.h"
#include "remote-port-sk.h"
};
#include "remote-port-tlm.h"
#include "remote-port-tlm-wires.h"
#include "util.h"
using namespace sc_core;
using namespace std;

remoteport_tlm_wires::remoteport_tlm_wires(sc_module_name name,unsigned int nr_wires_in,unsigned int nr_wires_out,bool posted_updates)
        : sc_module(name), wires_in("wires-in", nr_wires_in), wires_out("wires-out", nr_wires_out),
		wire_name(name)
{
	cfg.nr_wires_in = nr_wires_in;
	cfg.nr_wires_out = nr_wires_out;
	cfg.posted_updates = posted_updates;

	if ( wires_in.size() )
	{
		for (unsigned int i = 0; i < wires_in.size(); i++) 
			sensitive << wires_in[i];	
		SC_THREAD(wire_update);
	}
}

void remoteport_tlm_wires::cmd_interrupt_null(remoteport_tlm *adaptor, struct rp_pkt &pkt, bool can_sync, remoteport_tlm_wires *dev)
{
	struct rp_pkt lpkt = pkt;
	adaptor->sync->pre_wire_cmd(pkt.sync.timestamp, can_sync);
	if (dev) {
		dev->interrupt_action(pkt);
	}

	if (adaptor->peer.caps.wire_posted_updates && !(lpkt.hdr.flags & RP_PKT_FLAGS_posted)) {
		int64_t clk;
		size_t plen;
		unsigned int id = lpkt.hdr.id;

	    clk = adaptor->rp_map_time(adaptor->sync->get_current_time());
		plen = rp_encode_interrupt_f(lpkt.hdr.id,
					     lpkt.hdr.dev,
					     &lpkt.interrupt,
					     clk, lpkt.interrupt.line,
					     0, lpkt.interrupt.val,
					     lpkt.hdr.flags | RP_PKT_FLAGS_response);
		adaptor->rp_write(&lpkt, plen);
	}

	adaptor->sync->post_wire_cmd(pkt.sync.timestamp, can_sync);
}

void remoteport_tlm_wires::interrupt_action( struct rp_pkt &pkt)
{
	assert(pkt.hdr.dev == dev_id);
	assert(pkt.interrupt.line < cfg.nr_wires_out);
	wires_out[pkt.interrupt.line].write(pkt.interrupt.val);
}

void remoteport_tlm_wires::cmd_interrupt(struct rp_pkt &pkt, bool can_sync)
{
	cmd_interrupt_null(adaptor, pkt, can_sync, this);
}

#include <algorithm>
void remoteport_tlm_wires::wire_update(void)
{
	remoteport_packet pkt_tx;
	unsigned int ri;
	
	pkt_tx.alloc(sizeof pkt_tx.pkt->interrupt);

	while (true) 
	{
		
		wait();
		size_t nr_events{0};
		for_each( wires_in.begin(), wires_in.end(), [&]( sc_in<bool> wire ) { wire.event() ? nr_events++ : 0; } );

		uint32_t flags{RP_PKT_FLAGS_posted};
		uint32_t id{0};

		for (unsigned int i = 0; i < wires_in.size(); i++)
		{
			if ( wires_in[i].event() )
			{
				nr_events--;
				if (nr_events == 0 && !cfg.posted_updates)
					flags = 0;
				id = adaptor->rp_pkt_id++;
				int64_t clk = adaptor->rp_map_time(adaptor->sync->get_current_time());
				size_t plen = rp_encode_interrupt_f(id,dev_id,&pkt_tx.pkt->interrupt, clk, i, 0, wires_in[i].read(), flags);
				adaptor->rp_write(pkt_tx.pkt, plen);
			}
		}

		// Wait for an ACK on the last one.
		if (adaptor->peer.caps.wire_posted_updates && !(flags & RP_PKT_FLAGS_posted))
		{
			ri = response_wait(id);
			assert(resp[ri].pkt.pkt->hdr.id == id);
			response_done(ri);
		}

	}
}

template <class T>
static auto generic_tie_off( T & wires, const char* wire_name )
{
	char n_str[64];
	sc_signal<bool> *sig;

	for (unsigned int i = 0; i < wires.size(); i++) 
	{
		if (wires[i].size())
			continue;
		sprintf(n_str, "tie_off_%s_wires_in_%d", wire_name, i);
		sig = new sc_signal<bool>(n_str);
		wires[i](*sig);
	}

}

void remoteport_tlm_wires::tie_off(void)
{
	generic_tie_off( wires_in, wire_name );
	generic_tie_off( wires_out, wire_name );
}
